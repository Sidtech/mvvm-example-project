//
//  IssueDetailViewController.swift
//  MVVM
//
//  Create by SIdharth on 8/12/2021
//

import UIKit

class IssueDetailViewController: UIViewController {
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var stackView: UIStackView!
    @IBOutlet var labelTitleValue: UILabel!
    @IBOutlet var labelSubtitleValue: UILabel!
    @IBOutlet var buttonStatusValue: UIButton!
    @IBOutlet var labelDescriptionValue: UILabel!
    @IBOutlet var viewTags: UIView!
    
    lazy var viewModel = {
        IssueDetailViewModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        initViewModel()
    }
    
    func initView() {
        viewModel.updateUI = { [weak self] in
            guard let self = self else {return}
            DispatchQueue.main.async {
                self.labelTitleValue.text = self.viewModel.issue?.title
                var subtitleLabelStr = ""
                if let number = self.viewModel.issue?.number{
                    subtitleLabelStr = "#\(number)"
                }
                if let createdAt = self.viewModel.issue?.createdAt{
                    if let date = createdAt.iso8601{
                        subtitleLabelStr += " opened"
                        let dateStr = date.toStringWithRelativeTime()
                        subtitleLabelStr += " \(dateStr) "
                    }
                }
                if let username = self.viewModel.issue?.user?.login, !subtitleLabelStr.contains("opened"){
                    subtitleLabelStr += " opened"
                    subtitleLabelStr += " by \(username)"
                }else if let username = self.viewModel.issue?.user?.login{
                    subtitleLabelStr += " by \(username)"
                }
                self.labelSubtitleValue.text = subtitleLabelStr
                if let state = self.viewModel.issue?.state{
                    self.buttonStatusValue.setTitle(state.capitalized, for: .normal)
                    self.buttonStatusValue.backgroundColor = .systemGreen
                }
                self.labelDescriptionValue.text = self.viewModel.issue?.body
                self.addTags(self.viewTags,self.viewModel.issue?.labels)
            }
        }
    }
    
    func initViewModel() {
        // Get issues data
        viewModel.getIssue()
    }
    
    //MARK:- Display Category Tags UI
    func addTags(_ parentView : UIView,_ tagArray: [Label]?) {
        parentView.removeAllSubviews()
        let parentViewWidth = UIScreen.main.bounds.width - 60
        var rowWidthUsed = CGFloat()
        var lastTagLabelRef = UIButton()
        var catTagWidth = CGFloat()
        var tagsWrapperHeight = CGFloat()
        if tagArray?.count ?? 0 > 0{
            let categoryTagButton = UIButton()
            categoryTagButton.layer.cornerRadius = 13
            categoryTagButton.setTitle(tagArray?[0].name,for:.normal)
            categoryTagButton.contentEdgeInsets = UIEdgeInsets.init(top:5, left:10, bottom:5, right:10)
            categoryTagButton.titleLabel?.font = UIFont.systemFont(ofSize: 13)
            categoryTagButton.backgroundColor = UIColor.init(hex: tagArray?[0].color ?? "#FFFFFF")
            categoryTagButton.backgroundColor = UIColor.init(hex: "50" + (tagArray?[0].color ?? "#000000") )
            categoryTagButton.layer.borderWidth = 1
            categoryTagButton.layer.borderColor = UIColor.init(hex: tagArray?[0].color ?? "#FFFFFF")?.cgColor
            categoryTagButton.setTitleColor(UIColor.init(hex: tagArray?[0].color ?? "#FFFFFF"),for:.normal)
            parentView.addSubview(categoryTagButton)
            categoryTagButton.translatesAutoresizingMaskIntoConstraints = false
            categoryTagButton.topAnchor.constraint(equalTo: parentView.topAnchor, constant:10).isActive = true
            categoryTagButton.leadingAnchor.constraint(equalTo: parentView.leadingAnchor, constant:0).isActive = true
            catTagWidth = categoryTagButton.calculateWidth(padding:20)
            if(catTagWidth > parentViewWidth) {
                categoryTagButton.trailingAnchor.constraint(equalTo:parentView.trailingAnchor, constant:0).isActive = true
            }
            lastTagLabelRef = categoryTagButton
            rowWidthUsed = categoryTagButton.calculateWidth(padding:20)
            tagsWrapperHeight = categoryTagButton.calculateHeight(padding:10)
            if tagArray?.count == 1 {
                categoryTagButton.bottomAnchor.constraint(equalTo: parentView.bottomAnchor, constant:-10).isActive = true
                return
            }
            for i in 2...(tagArray?.count ?? 0) {
                let newCategoryTagButton = UIButton()
                newCategoryTagButton.tag = i
                newCategoryTagButton.layer.cornerRadius = 13
                newCategoryTagButton.setTitle(tagArray?[i-1].name,for:.normal)
                newCategoryTagButton.contentEdgeInsets = UIEdgeInsets.init(top:5, left:10, bottom:5, right:10)
                newCategoryTagButton.titleLabel?.font = UIFont.systemFont(ofSize: 13)
                newCategoryTagButton.backgroundColor = UIColor.init(hex: "50" + (tagArray?[i-1].color ?? "#000000"))
                newCategoryTagButton.layer.borderWidth = 1
                newCategoryTagButton.layer.borderColor = UIColor.init(hex: tagArray?[i-1].color ?? "#FFFFFF")?.cgColor
                newCategoryTagButton.setTitleColor(UIColor.init(hex: tagArray?[i-1].color ?? "#FFFFFF"),for:.normal)
                parentView.addSubview(newCategoryTagButton)
                newCategoryTagButton.translatesAutoresizingMaskIntoConstraints = false
                catTagWidth = newCategoryTagButton.calculateWidth(padding:30)
                if ((rowWidthUsed + catTagWidth) <= parentViewWidth ) {
                    newCategoryTagButton.topAnchor.constraint(equalTo: lastTagLabelRef.topAnchor, constant:0).isActive = true
                    newCategoryTagButton.leadingAnchor.constraint(equalTo: lastTagLabelRef.trailingAnchor, constant:5).isActive = true
                    rowWidthUsed = rowWidthUsed + catTagWidth
                } else {
                    newCategoryTagButton.topAnchor.constraint(equalTo: lastTagLabelRef.bottomAnchor, constant:5).isActive = true
                    newCategoryTagButton.leadingAnchor.constraint(equalTo: parentView.leadingAnchor, constant:0).isActive = true
                    rowWidthUsed = catTagWidth
                    tagsWrapperHeight = tagsWrapperHeight + categoryTagButton.calculateHeight(padding:5)
                }
                if(catTagWidth > parentViewWidth) {
                    newCategoryTagButton.trailingAnchor.constraint(equalTo: parentView.trailingAnchor, constant:0).isActive = true
                }
                if i == (tagArray?.count) {
                    newCategoryTagButton.bottomAnchor.constraint(equalTo: parentView.bottomAnchor, constant:-10).isActive = true
                    //categoryTagWrapperHeightConstraint.constant = tagsWrapperHeight
                }
                lastTagLabelRef = newCategoryTagButton
            }
        }else{
            parentView.heightAnchor.constraint(equalToConstant: 0).isActive = true
        }
    }
}

