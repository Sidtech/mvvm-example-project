//
//  IssueDetailViewModel.swift
//  MVVM
//
//  Create by SIdharth on 8/12/2021
//

import Foundation
import UIKit

class IssueDetailViewModel: NSObject {
    private var githubService: GithubServiceProtocol
    
    var number: Int?
    
    var updateUI: (() -> Void)?
    
    var issue : Issue?

    init(githubService: GithubServiceProtocol = GithubService()) {
        self.githubService = githubService
    }
    
    func getIssue() {
        guard let number = number else {
            return
        }
        githubService.getIssue(number:  String(number)) { success, model, error in
            if success, let issue = model {
                self.fetchData(issue: issue)
            } else {
                print(error!)
            }
        }
    }
    
    func fetchData(issue: Issue) {
        self.issue = issue // Cache
        self.updateUI?()
    }
}
