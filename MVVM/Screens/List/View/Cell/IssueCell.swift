//
//  IssueCell.swift
//  MVVM
//
//  Create by SIdharth on 8/12/2021
//

import UIKit

class IssueCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    
    class var identifier: String { return String(describing: self) }
    class var nib: UINib { return UINib(nibName: identifier, bundle: nil) }
    
    var cellViewModel: IssueCellViewModel? {
        didSet {
            var subtitleLabelStr = ""
            if let number = cellViewModel?.number{
                subtitleLabelStr = "#\(number)"
            }
            if let createdAt = cellViewModel?.createdAt{
                if let date = createdAt.iso8601{
                    subtitleLabelStr += " opened"
                    let dateStr = date.toStringWithRelativeTime()
                    subtitleLabelStr += " \(dateStr) "
                }
            }
            if let username = cellViewModel?.username, !subtitleLabelStr.contains("opened"){
                subtitleLabelStr += " opened"
                subtitleLabelStr += " by \(username)"
            }else if let username = cellViewModel?.username{
                subtitleLabelStr += " by \(username)"
            }
            
            subtitleLabel.text = subtitleLabelStr
            titleLabel.text = cellViewModel?.title
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initView()
    }
    
    func initView() {
        // Cell view customization
        backgroundColor = .clear
        
        // Line separator full width
        preservesSuperviewLayoutMargins = false
        separatorInset = UIEdgeInsets.zero
        layoutMargins = UIEdgeInsets.zero
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = nil
        subtitleLabel.text = nil
    }
}
