//
//  IssueListViewController.swift
//  MVVM
//
//  Create by SIdharth on 8/12/2021
//

import UIKit

class IssueListViewController: UIViewController {
    @IBOutlet var tableView: UITableView!

    // MARK: - Routes
    enum ListRoute: String {
        case detail
    }
    
    // MARK: - Variables
    var viewModel: IssuesViewModel!
    var router: ListRouter!
    
    // MARK: - init(s)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        router = DefaultListRouter(viewController: self)
        viewModel = IssuesViewModel()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        initViewModel()
    }

    func initView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .black
        tableView.separatorColor = .white
        tableView.separatorStyle = .singleLine
        tableView.tableFooterView = UIView()
        tableView.register(IssueCell.nib, forCellReuseIdentifier: IssueCell.identifier)
    }

    func initViewModel() {
        // Get issues data
        viewModel.getIssues()
        
        // Reload TableView closure
        viewModel.reloadTableView = { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        router.prepare(for: segue)
    }
}

// MARK: - UITableViewDelegate

extension IssueListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        viewModel.setSelectedIssue(at: indexPath, completion:{
            router.navigate(to: .detail)
        })
    }
}

// MARK: - UITableViewDataSource

extension IssueListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.issueCellViewModels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: IssueCell.identifier, for: indexPath) as? IssueCell else { fatalError("xib does not exists") }
        let cellVM = viewModel.getCellViewModel(at: indexPath)
        cell.cellViewModel = cellVM
        return cell
    }
}
