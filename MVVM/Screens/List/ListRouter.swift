//
//  ListRouter.swift
//  MVVM
//
//  Create by SIdharth on 8/12/2021
//

import UIKit

protocol ListRouter {
    typealias Route = IssueListViewController.ListRoute
    
    var viewController: IssueListViewController! { get }
    init(viewController: IssueListViewController)
    
    func navigate(to route: Route)
    func prepare(for segue: UIStoryboardSegue)
}

class DefaultListRouter: ListRouter {
    weak var viewController: IssueListViewController!
    
    required init(viewController: IssueListViewController) {
        self.viewController = viewController
    }
    
    func navigate(to route: Route) {
        viewController.performSegue(withIdentifier: route.rawValue, sender: nil)
    }
    
    func prepare(for segue: UIStoryboardSegue) {
        guard let segueID = segue.identifier,
            let route = IssueListViewController.ListRoute(rawValue: segueID)  else {
            return
        }
        
        switch route {
        case .detail:
            passDataToDetail(segue)
        }
    }
}

// MARK: - extension for passing data logic
extension DefaultListRouter {
    private func passDataToDetail(_ segue: UIStoryboardSegue) {
        let detailVC = segue.destination as! IssueDetailViewController
        detailVC.viewModel.number = viewController.viewModel.selectedIssue?.number
    }
}
