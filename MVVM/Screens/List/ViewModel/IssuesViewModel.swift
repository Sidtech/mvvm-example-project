//
//  IssuesViewModel.swift
//  MVVM
//
//  Create by SIdharth on 8/12/2021
//

import Foundation

class IssuesViewModel: NSObject {
    private var githubService: GithubServiceProtocol
    
    var reloadTableView: (() -> Void)?
    
    var issues = Issues()
    
    var selectedIssue : Issue?
    
    var issueCellViewModels = [IssueCellViewModel]() {
        didSet {
            reloadTableView?()
        }
    }

    init(githubService: GithubServiceProtocol = GithubService()) {
        self.githubService = githubService
    }
    
    func getIssues() {
        githubService.getIssues { success, model, error in
            if success, let issues = model {
                self.fetchData(issues: issues)
            } else {
                print(error!)
            }
        }
    }
    
    func fetchData(issues: Issues) {
        self.issues = issues // Cache
        var vms = [IssueCellViewModel]()
        for issue in issues {
            vms.append(createCellModel(issue: issue))
        }
        issueCellViewModels = vms
    }
        
    func createCellModel(issue: Issue) -> IssueCellViewModel {
        let number = issue.number
        let title = issue.title
        let createdAt = issue.createdAt
        let username = issue.user?.login
        
        return IssueCellViewModel(number: number, title: title, createdAt: createdAt, username: username)
    }
    
    func getCellViewModel(at indexPath: IndexPath) -> IssueCellViewModel {
        return issueCellViewModels[indexPath.row]
    }
    
    func setSelectedIssue(at indexPath: IndexPath,completion: () -> Void) {
        self.selectedIssue = self.issues[indexPath.row]
        completion()
    }
}
