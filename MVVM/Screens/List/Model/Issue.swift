//
//	Issue.swift
//
//	Create by SIdharth on 8/12/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

typealias Issues = [Issue]

struct Issue : Codable {

	let activeLockReason : String?
	let assignee : User?
	let assignees : [User]?
	let authorAssociation : String?
	let body : String?
	let closedAt : String?
	let comments : Int?
	let commentsUrl : String?
	let createdAt : String?
	let draft : Bool?
	let eventsUrl : String?
	let htmlUrl : String?
	let id : Int?
	let labels : [Label]?
	let labelsUrl : String?
	let locked : Bool?
	let milestone : String?
	let nodeId : String?
	let number : Int?
	let performedViaGithubApp : String?
	let pullRequest : PullRequest?
	let reactions : Reaction?
	let repositoryUrl : String?
	let state : String?
	let timelineUrl : String?
	let title : String?
	let updatedAt : String?
	let url : String?
	let user : User?


	enum CodingKeys: String, CodingKey {
		case activeLockReason = "active_lock_reason"
		case assignee = "assignee"
		case assignees = "assignees"
		case authorAssociation = "author_association"
		case body = "body"
		case closedAt = "closed_at"
		case comments = "comments"
		case commentsUrl = "comments_url"
		case createdAt = "created_at"
		case draft = "draft"
		case eventsUrl = "events_url"
		case htmlUrl = "html_url"
		case id = "id"
		case labels = "labels"
		case labelsUrl = "labels_url"
		case locked = "locked"
		case milestone = "milestone"
		case nodeId = "node_id"
		case number = "number"
		case performedViaGithubApp = "performed_via_github_app"
		case pullRequest
		case reactions
		case repositoryUrl = "repository_url"
		case state = "state"
		case timelineUrl = "timeline_url"
		case title = "title"
		case updatedAt = "updated_at"
		case url = "url"
		case user
	}
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		activeLockReason = try values.decodeIfPresent(String.self, forKey: .activeLockReason)
		assignee = try values.decodeIfPresent(User.self, forKey: .assignee)
		assignees = try values.decodeIfPresent([User].self, forKey: .assignees)
		authorAssociation = try values.decodeIfPresent(String.self, forKey: .authorAssociation)
		body = try values.decodeIfPresent(String.self, forKey: .body)
		closedAt = try values.decodeIfPresent(String.self, forKey: .closedAt)
		comments = try values.decodeIfPresent(Int.self, forKey: .comments)
		commentsUrl = try values.decodeIfPresent(String.self, forKey: .commentsUrl)
		createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt)
		draft = try values.decodeIfPresent(Bool.self, forKey: .draft)
		eventsUrl = try values.decodeIfPresent(String.self, forKey: .eventsUrl)
		htmlUrl = try values.decodeIfPresent(String.self, forKey: .htmlUrl)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		labels = try values.decodeIfPresent([Label].self, forKey: .labels)
		labelsUrl = try values.decodeIfPresent(String.self, forKey: .labelsUrl)
		locked = try values.decodeIfPresent(Bool.self, forKey: .locked)
		milestone = try values.decodeIfPresent(String.self, forKey: .milestone)
		nodeId = try values.decodeIfPresent(String.self, forKey: .nodeId)
		number = try values.decodeIfPresent(Int.self, forKey: .number)
		performedViaGithubApp = try values.decodeIfPresent(String.self, forKey: .performedViaGithubApp)
		pullRequest = try PullRequest(from: decoder)
		reactions = try Reaction(from: decoder)
		repositoryUrl = try values.decodeIfPresent(String.self, forKey: .repositoryUrl)
		state = try values.decodeIfPresent(String.self, forKey: .state)
		timelineUrl = try values.decodeIfPresent(String.self, forKey: .timelineUrl)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt)
		url = try values.decodeIfPresent(String.self, forKey: .url)
		user = try values.decodeIfPresent(User.self, forKey: .user)
	}


}
