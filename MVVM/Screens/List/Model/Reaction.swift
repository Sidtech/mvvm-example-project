//
//	Reaction.swift
//
//	Create by SIdharth on 8/12/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct Reaction : Codable {

	let plusOne : Int?
    let minusOne : Int?
	let confused : Int?
	let eyes : Int?
	let heart : Int?
	let hooray : Int?
	let laugh : Int?
	let rocket : Int?
	let totalCount : Int?
	let url : String?


	enum CodingKeys: String, CodingKey {
		case plusOne = "+1"
        case minusOne = "-1"
		case confused = "confused"
		case eyes = "eyes"
		case heart = "heart"
		case hooray = "hooray"
		case laugh = "laugh"
		case rocket = "rocket"
		case totalCount = "total_count"
		case url = "url"
	}
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
        plusOne = try values.decodeIfPresent(Int.self, forKey: .plusOne)
        minusOne = try values.decodeIfPresent(Int.self, forKey: .minusOne)
		confused = try values.decodeIfPresent(Int.self, forKey: .confused)
		eyes = try values.decodeIfPresent(Int.self, forKey: .eyes)
		heart = try values.decodeIfPresent(Int.self, forKey: .heart)
		hooray = try values.decodeIfPresent(Int.self, forKey: .hooray)
		laugh = try values.decodeIfPresent(Int.self, forKey: .laugh)
		rocket = try values.decodeIfPresent(Int.self, forKey: .rocket)
		totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
		url = try values.decodeIfPresent(String.self, forKey: .url)
	}


}
