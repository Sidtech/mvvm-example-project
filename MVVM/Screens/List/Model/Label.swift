//
//	Label.swift
//
//	Create by SIdharth on 8/12/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct Label : Codable {

	let color : String?
	let defaultField : Bool?
	let descriptionField : String?
	let id : Int?
	let name : String?
	let nodeId : String?
	let url : String?


	enum CodingKeys: String, CodingKey {
		case color = "color"
		case defaultField = "default"
		case descriptionField = "description"
		case id = "id"
		case name = "name"
		case nodeId = "node_id"
		case url = "url"
	}
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		color = try values.decodeIfPresent(String.self, forKey: .color)
		defaultField = try values.decodeIfPresent(Bool.self, forKey: .defaultField)
		descriptionField = try values.decodeIfPresent(String.self, forKey: .descriptionField)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		nodeId = try values.decodeIfPresent(String.self, forKey: .nodeId)
		url = try values.decodeIfPresent(String.self, forKey: .url)
	}


}