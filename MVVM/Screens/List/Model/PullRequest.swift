//
//	PullRequest.swift
//
//	Create by SIdharth on 8/12/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct PullRequest : Codable {

	let diffUrl : String?
	let htmlUrl : String?
	let mergedAt : String?
	let patchUrl : String?
	let url : String?


	enum CodingKeys: String, CodingKey {
		case diffUrl = "diff_url"
		case htmlUrl = "html_url"
		case mergedAt = "merged_at"
		case patchUrl = "patch_url"
		case url = "url"
	}
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		diffUrl = try values.decodeIfPresent(String.self, forKey: .diffUrl)
		htmlUrl = try values.decodeIfPresent(String.self, forKey: .htmlUrl)
		mergedAt = try values.decodeIfPresent(String.self, forKey: .mergedAt)
		patchUrl = try values.decodeIfPresent(String.self, forKey: .patchUrl)
		url = try values.decodeIfPresent(String.self, forKey: .url)
	}


}