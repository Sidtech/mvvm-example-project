//
//  GithubService.swift
//  MVVM
//
//  Create by SIdharth on 8/12/2021
//

import Foundation

protocol GithubServiceProtocol {
    func getIssues(completion: @escaping (_ success: Bool, _ results: Issues?, _ error: String?) -> ())
    func getIssue(number : String,completion: @escaping (_ success: Bool, _ results: Issue?, _ error: String?) -> ())
}

class GithubService: GithubServiceProtocol {
    
    func getIssues(completion: @escaping (Bool, Issues?, String?) -> ()) {
        HttpRequestHelper().GET(url: "https://api.github.com/repos/facebook/react/issues", params: ["": ""], httpHeader: .application_json) { success, data in
            if success {
                do {
                    let model = try JSONDecoder().decode(Issues.self, from: data!)
                    completion(true, model, nil)
                } catch {
                    completion(false, nil, "Error: Trying to parse Issues to model")
                }
            } else {
                completion(false, nil, "Error: Issues GET Request failed")
            }
        }
    }
    
    func getIssue(number : String,completion: @escaping (Bool, Issue?, String?) -> ()) {
        HttpRequestHelper().GET(url: "https://api.github.com/repos/facebook/react/issues/\(number)", params: ["": ""], httpHeader: .application_json) { success, data in
            if success {
                do {
                    let model = try JSONDecoder().decode(Issue.self, from: data!)
                    completion(true, model, nil)
                } catch {
                    completion(false, nil, "Error: Trying to parse Issues to model")
                }
            } else {
                completion(false, nil, "Error: Issues GET Request failed")
            }
        }
    }
}
